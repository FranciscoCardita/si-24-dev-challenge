import { useState, useEffect } from "react";

import Card from "../components/Card";
import Button from "../components/Button";
import Modal from "../components/Modal";
import RestaurantForm from "../components/RestaurantForm";

import { getRestaurants, createRestaurant } from "../api/api";
import { RestaurantType } from "../types/entities";
import { NavLink } from "react-router-dom";

const Restaurants = () => {
    const [restaurants, setRestaurants] = useState<RestaurantType[]>([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [searchQuery, setSearchQuery] = useState("");
    const [sort, setSort] = useState("");

    useEffect(() => {
        listRestaurants();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchQuery, sort]);

    const listRestaurants = async () => {
        try {
            const { data } = await getRestaurants(searchQuery, sort);
            setRestaurants(data);
        } catch {
            console.log("Error");
        }
    }

    const addRestaurant = async (formData: FormData) => {
        try {
            await createRestaurant(formData);
            listRestaurants();
        } catch {
            console.log("Error");
        }
    }

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(e.target.value)
    }

    const toggleSort = () => {
        if (sort === "rating") {
            setSort("-rating");
        } else {
            setSort("rating");
        }
    }

    const getSortIcon = () => {
        if (sort === "rating") {
            return "↑";
        } else if (sort === "-rating") {
            return "↓";
        } else {
            return "";
        }
    }

    return (
        <div className="layout h-[94vh] justify-start relative">
            <div className="flex justify-between items-center mb-4">
                <div className="flex space-x-8 items-center">
                    <h3>Restaurants</h3>
                    <input
                        type="text"
                        placeholder="Search restaurants..."
                        value={searchQuery}
                        onChange={handleSearch}
                        className="search-input"
                    />
                </div>
                <div className="space-x-2">
                    <Button onClick={() => { toggleSort() }}>
                        Sort by rating {getSortIcon()}
                    </Button>
                    <Button variant="outlined" onClick={() => setIsModalOpen(true)}>
                        + Add restaurant
                    </Button>
                </div>
            </div>
            <div className="grid grid-cols-4 gap-4">
                {restaurants.map((restaurant, index) => (
                    <NavLink key={restaurant.id} to={`/restaurants/${restaurant.id}`}>
                        <Card
                            key={index}
                            title={restaurant.name}
                            description={`${restaurant.city}, ${restaurant.country}`}
                            rating={restaurant.rating}
                        />
                    </NavLink>
                ))}
            </div>
            <Modal title="Add restaurant" isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
                <RestaurantForm onSubmit={(formData) => { addRestaurant(formData) }} onClose={() => setIsModalOpen(false)} />
            </Modal>
        </div >
    );
}

export default Restaurants