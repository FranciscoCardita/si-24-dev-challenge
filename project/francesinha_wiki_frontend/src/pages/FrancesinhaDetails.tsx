import { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom"

import Button from "../components/Button";
import Rating from "../components/Rating";
import FrancesinhaForm from "../components/FrancesinhaForm";
import { FrancesinhaType } from "../types/entities";
import { getFrancesinha, deleteFrancesinha, updateFrancesinha } from "../api/api";

import defaultImg from "../assets/default.png"
import Modal from "../components/Modal";

const FrancesinhaDetails = () => {
    const { id } = useParams<'id'>()
    const [francesinha, setFrancesinha] = useState<FrancesinhaType | null>(null)
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        getFrancesinhaDetails()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id])

    const getFrancesinhaDetails = async () => {
        try {
            const { data } = await getFrancesinha(id as string)
            setFrancesinha(data)
        } catch {
            console.log("Error")
        }
    }

    const editFrancesinha = async (formData: FormData) => {
        try {
            await updateFrancesinha(id as string, formData)
            window.location.reload()
        } catch {
            console.log("Error")
        }
    }

    const deleteFrancesinhaHandler = async () => {
        try {
            await deleteFrancesinha(id as string)
            navigate({ pathname: '/francesinhas' })
        } catch {
            console.log("Error")
        }
    }

    return (
        <div className="layout h-[94vh] justify-start relative">
            <div className="mt-1 flex justify-between items-center">
                <Button
                    variant="contained"
                    onClick={() => navigate({ pathname: '/francesinhas' })}
                    className=""
                >Back to Francesinhas</Button>
                <div className="space-x-2">
                    <Button variant="outlined" className="" onClick={() => { setIsModalOpen(true) }}>Edit</Button>
                    <Button variant="contained" className="bg-red-500 hover:bg-red-600" onClick={() => setIsConfirmationModalOpen(true)}>Delete</Button>
                </div>
            </div>
            <div>
                {francesinha && (
                    <div className="flex justify-between">
                        <div>
                            <h1>{francesinha.name}</h1>
                            <div className="space-y-4">
                                <h4 className="font-normal">{`Price: €${francesinha.price}`}</h4>
                                <Rating rating={francesinha.rating} />
                                <div className="flex">
                                    <h4 className="font-normal">Restaurant:</h4>
                                    <NavLink className="flex" to={`/restaurants/${francesinha.restaurant.id}`}>
                                        &nbsp; &nbsp;
                                        <h4 className="font-normal underline">{francesinha.restaurant.name}</h4>
                                    </NavLink>
                                </div>
                                <div className="flex">
                                    <h4 className="font-normal">Ingredients:&nbsp;</h4>
                                    <div className="flex flex-wrap gap-x-8">
                                        {francesinha.ingredients.map((ingredient, index) => (
                                            <h4 key={index} className="font-normal">{ingredient.name}</h4>
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <img src={francesinha.photos == null ? defaultImg : francesinha.photos} alt={francesinha.name} className="mt-6 w-1/2 h-96 object-cover justify-end" />
                    </div>
                )}
            </div>
            <Modal title="Edit Francesinha" isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
                <FrancesinhaForm initialData={francesinha} onSubmit={(formData) => { editFrancesinha(formData) }} onClose={() => setIsModalOpen(false)} />
            </Modal>
            <Modal title="Delete Francesinha" isOpen={isConfirmationModalOpen} onClose={() => setIsConfirmationModalOpen(false)}>
                <div className="flex flex-col space-y-4">
                    <span className="font-normal text-xl">Are you sure you want to delete this francesinha?</span>
                    <div className="flex justify-end space-x-4">
                        <Button onClick={() => setIsConfirmationModalOpen(false)}>Cancel</Button>
                        <Button variant="contained" className="bg-red-500 hover:bg-red-600" onClick={deleteFrancesinhaHandler}>Delete</Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default FrancesinhaDetails