import { useState, useEffect } from "react"
import { NavLink, useNavigate } from "react-router-dom"

import Card from "../components/Card"
import Button from "../components/Button"
import Modal from "../components/Modal"
import FrancesinhaForm from "../components/FrancesinhaForm"

import { getFrancesinhas, createFrancesinha } from "../api/api"
import { FrancesinhaType } from "../types/entities"
import defaultImg from "../assets/default.png"

const Francesinhas = () => {
    const [francesinhas, setFrancesinhas] = useState<FrancesinhaType[]>([])
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [searchQuery, setSearchQuery] = useState("")
    const [sort, setSort] = useState("")
    const navigate = useNavigate()

    useEffect(() => {
        listFrancesinhas()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchQuery, sort])

    const listFrancesinhas = async () => {
        try {
            const { data } = await getFrancesinhas(searchQuery, sort)
            setFrancesinhas(data)
        } catch {
            console.log("Error")
        }
    }

    const addFrancesinha = async (formData: FormData) => {
        try {
            await createFrancesinha(formData)
            listFrancesinhas()
        } catch {
            console.log("Error")
        }
    }

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(e.target.value)
    }

    const toggleSort = () => {
        if (sort === "rating") {
            setSort("-rating")
        } else {
            setSort("rating")
        }
    }

    const getSortIcon = () => {
        if (sort === "rating") {
            return "↑"
        } else if (sort === "-rating") {
            return "↓"
        } else {
            return ""
        }
    }

    return (
        <div className="layout h-[94vh] justify-start relative">
            <div className="flex justify-between items-center mb-4">
                <div key='header' className="flex space-x-8 items-center">
                    <h3>Francesinhas</h3>
                    <input
                        type="text"
                        placeholder="Search francesinhas..."
                        value={searchQuery}
                        onChange={handleSearch}
                        className="search-input"
                    />
                </div>
                <div key='buttons' className="space-x-2">
                    <Button onClick={toggleSort}>
                        Sort by rating {getSortIcon()}
                    </Button>
                    <Button variant="outlined" onClick={() => setIsModalOpen(true)}>
                        + Add francesinha
                    </Button>
                    <Button variant="outlined" onClick={() => { navigate({ pathname: '/francesinhas/ingredients/' }) }}>
                        Ingredients
                    </Button>
                </div>
            </div>
            <div className="grid grid-cols-4 gap-4">
                {francesinhas.map((francesinha, index) => (
                    <NavLink key={francesinha.id} to={`/francesinhas/${francesinha.id as string}`}>
                        <Card
                            key={index}
                            title={francesinha.name}
                            description={`€${francesinha.price}\n${francesinha.restaurant.name}`}
                            rating={francesinha.rating}
                            image={francesinha.photos || defaultImg}
                        />
                    </NavLink>
                ))}
            </div>
            <Modal title="Add Francesinha" isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
                <FrancesinhaForm onSubmit={(formData) => { addFrancesinha(formData) }} onClose={() => setIsModalOpen(false)} />
            </Modal>
        </div>
    )
}

export default Francesinhas