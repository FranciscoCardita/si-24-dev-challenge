import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import Button from "../components/Button";
import { IngredientType } from "../types/entities";
import { getIngredients, createIngredient, updateIngredient, deleteIngredient } from "../api/api";
import Modal from "../components/Modal";
import IngredientForm from "../components/IngredientForm";

const Ingredients = () => {
    const [ingredients, setIngredients] = useState<IngredientType[]>([]);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
    const [selectedIngredient, setSelectedIngredient] = useState<IngredientType | null>(null);
    const [searchQuery, setSearchQuery] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        listIngredients();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchQuery]);

    const listIngredients = async () => {
        try {
            const { data } = await getIngredients(searchQuery);
            setIngredients(data);
        } catch {
            console.log('Error listing ingredients');
        }
    }

    const addIngredient = async (formData: FormData) => {
        try {
            await createIngredient(formData);
            listIngredients();
            setIsModalOpen(false);
        } catch {
            console.log('Error creating ingredient');
        }
    }

    const editIngredient = async (formData: FormData) => {
        try {
            await updateIngredient(selectedIngredient?.id?.toString() ?? '', formData);
            listIngredients();
            setSelectedIngredient(null);
            setIsModalOpen(false);
        } catch {
            console.log('Error updating ingredient');
        }
    }

    const deleteIngredientHandler = async () => {
        try {
            await deleteIngredient(selectedIngredient?.id?.toString() ?? '');
            listIngredients();
            setIsConfirmationModalOpen(false);
        } catch {
            console.log('Error deleting ingredient');
        }
    }

    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearchQuery(e.target.value);
    }

    return (
        <div className="layout h-[94vh] justify-start relative space-y-2">
            <div className="mt-2 mb-4 flex justify-between items-center">
                <div className="space-x-2">
                    <Button
                        variant="contained"
                        onClick={() => navigate({ pathname: '/francesinhas' })}
                        className=""
                    >Back to Francesinhas</Button>
                    <input
                        type="text"
                        placeholder="Search ingredients..."
                        value={searchQuery}
                        onChange={handleSearch}
                        className="search-input"
                    />
                </div>
                <Button
                    variant="outlined"
                    onClick={() => { setIsModalOpen(true) }}
                    className=""
                >+ Add Ingredient</Button>
            </div>
            <div className="w-full space-y-2">
                {ingredients.map((ingredient) => (
                    <div key={ingredient.id} className="border-2 border-amber-400 p-4 rounded-lg shadow-md flex justify-between items-center">
                        <h4>{ingredient.name}</h4>
                        <div className="flex justify-between mt-2 space-x-2">
                            <Button
                                variant="outlined"
                                onClick={() => {
                                    setSelectedIngredient(ingredient);
                                    setIsModalOpen(true);
                                }}
                                className=""
                            >Edit</Button>
                            <Button
                                variant="contained"
                                onClick={() => {
                                    setSelectedIngredient(ingredient);
                                    setIsConfirmationModalOpen(true);
                                }}
                                className="bg-red-500 hover:bg-red-600"
                            >Delete</Button>
                        </div>
                    </div>
                ))}
            </div>
            <Modal title="Delete ingredient" isOpen={isConfirmationModalOpen} onClose={() => setIsConfirmationModalOpen(false)}>
                <div className="flex flex-col space-y-4">
                    <span className="font-normal text-xl">Are you sure you want to delete this ingredient?</span>
                    <div className="flex justify-end space-x-4">
                        <Button variant="outlined" onClick={() => setIsConfirmationModalOpen(false)}>Cancel</Button>
                        <Button variant="contained" onClick={() => { deleteIngredientHandler() }} className="bg-red-500 hover:bg-red-600">Delete</Button>
                    </div>
                </div>
            </Modal>
            <Modal title={selectedIngredient ? "Edit Ingredient" : "Add Ingredient"} isOpen={isModalOpen} onClose={() => { setIsModalOpen(false); setSelectedIngredient(null) }}>
                <IngredientForm
                    initialData={selectedIngredient}
                    onSubmit={(formData) => { selectedIngredient ? editIngredient(formData) : addIngredient(formData) }}
                    onClose={() => setIsModalOpen(false)}
                />
            </Modal>
        </div>
    );
}

export default Ingredients;