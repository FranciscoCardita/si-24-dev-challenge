import { useNavigate } from "react-router-dom"

import Button from "../components/Button"

import francesinha from "../assets/francesinha.jpg"

const Home = () => {
    const navigate = useNavigate();
    const navigateToFrancesinhas = () => navigate('/francesinhas');

    return (
        <div className="layout h-[94vh] flex justify-center items-center relative">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                version="1.1"
                xmlnsXlink="http://www.w3.org/1999/xlink"
                viewBox="0 0 800 800"
                className="absolute z-0 left-0"
                style={{ width: "50%", height: "100%", opacity: 0.45 }}
            >
                <defs>
                    <filter id="bbblurry-filter" x="-100%" y="-100%" width="400%" height="400%" filterUnits="objectBoundingBox">
                        <feGaussianBlur stdDeviation="40" x="0%" y="0%" width="100%" height="100%" in="SourceGraphic" edgeMode="none" result="blur"></feGaussianBlur>
                    </filter>
                </defs>
                <g filter="url(#bbblurry-filter)">
                    <ellipse rx="150" ry="150" cx="398.30746475938736" cy="324.61869532400397" fill="#6cbfcb"></ellipse>
                    <ellipse rx="150" ry="150" cx="483.7905209526343" cy="358.5986257822726" fill="#8ed7b1"></ellipse>
                    <ellipse rx="150" ry="150" cx="318.6576197759018" cy="541.098514576857" fill="#ffa12e"></ellipse>
                </g>
            </svg>
            <div className="flex flex-col justify-center text-center w-1/2 relative z-10">
                <h1 className="text-start">Welcome to the <br /> Francesinha Wiki</h1>
                <h4 className="text-start mt-4">Discover the best Francesinhas and where to eat them.</h4>
                <Button variant="contained" className="mt-6 w-48" onClick={navigateToFrancesinhas}>
                    <p className="text-xl">Discover</p>
                </Button>
            </div>
            <div className="w-1/2 h-full rounded-e-xl bg-cover bg-center" style={{ backgroundImage: `url(${francesinha})` }}></div>
        </div >
    )
}

export default Home