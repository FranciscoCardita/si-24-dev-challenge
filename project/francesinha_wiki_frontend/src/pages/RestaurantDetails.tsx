import { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom"

import { RestaurantType } from "../types/entities";
import { getRestaurant, updateRestaurant, deleteRestaurant } from "../api/api";
import Button from "../components/Button";
import Rating from "../components/Rating";
import Modal from "../components/Modal";
import RestaurantForm from "../components/RestaurantForm";

const RestaurantDetails = () => {
    const { id } = useParams<'id'>();
    const [restaurant, setRestaurant] = useState<RestaurantType | null>(null)
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false)
    const navigate = useNavigate()

    useEffect(() => {
        getRestaurantDetails()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id])

    const getRestaurantDetails = async () => {
        try {
            const { data } = await getRestaurant(id as string)
            setRestaurant(data)
        } catch {
            console.log("Error")
        }
    }

    const editRestaurant = async (formData: FormData) => {
        try {
            await updateRestaurant(id as string, formData)
            window.location.reload()
        } catch {
            console.log("Error")
        }
    }

    const deleteRestaurantHandler = async () => {
        try {
            await deleteRestaurant(id as string)
            navigate({ pathname: '/restaurants' })
        } catch {
            console.log("Error")
        }
    }

    return (
        <div className="layout h-[94vh] justify-start relative">
            <div className="mt-1 flex justify-between items-center">
                <Button
                    variant="contained"
                    onClick={() => navigate({ pathname: '/restaurants' })}
                    className=""
                >Back to Restaurants</Button>
                <div className="space-x-2">
                    <Button variant="outlined" onClick={() => setIsModalOpen(true)}>Edit</Button>
                    <Button variant="contained" onClick={() => setIsConfirmationModalOpen(true)} className="bg-red-500 hover:bg-red-600">Delete</Button>
                </div>
            </div>
            <div>
                {restaurant && (
                    <div>
                        <h1>{restaurant.name}</h1>
                        <div className="space-y-4">
                            <Rating rating={restaurant.rating} />
                            <h4 className="font-normal">{`Address: ${restaurant.address}`}</h4>
                            <h4 className="font-normal">{`City: ${restaurant.city}, ${restaurant.country}`}</h4>
                            <div className="flex">
                                <h4 className="font-normal">Francesinhas:&nbsp;</h4>
                                <div className="flex flex-wrap gap-x-8">
                                    {restaurant.francesinhas.map((francesinha, index) => (
                                        <NavLink className="flex" key={index} to={`/francesinhas/${francesinha.id as string}`}>
                                            <h4 className="font-normal underline">{francesinha.name}</h4>
                                        </NavLink>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
            <Modal title="Edit restaurant" isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
                <RestaurantForm initialData={restaurant} onSubmit={(formData) => { editRestaurant(formData) }} onClose={() => setIsModalOpen(false)} />
            </Modal>
            <Modal title="Delete restaurant" isOpen={isConfirmationModalOpen} onClose={() => setIsConfirmationModalOpen(false)}>
                <div className="flex flex-col space-y-4">
                    <span className="font-normal text-xl">Are you sure you want to delete this restaurant?</span>
                    <div className="flex justify-end space-x-4">
                        <Button variant="outlined" onClick={() => setIsConfirmationModalOpen(false)}>Cancel</Button>
                        <Button variant="contained" onClick={() => { deleteRestaurantHandler() }} className="bg-red-500 hover:bg-red-600">Delete</Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default RestaurantDetails