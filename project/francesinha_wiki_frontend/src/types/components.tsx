export type ButtonType = {
    children?: React.ReactNode;
    variant?: 'contained' | 'outlined' | 'text';
    type?: 'button' | 'submit' | 'reset';
    onClick?: () => void;
    className?: string;
};

export type CardProps = {
    title?: string;
    description?: string;
    rating?: number;
    image?: string;
    onClick?: () => void;
};

export type CircleIconType = {
    filled?: boolean;
    halfFilled?: boolean;
    className?: string;
};

export type ModalType = {
    isOpen?: boolean;
    title?: string;
    children?: React.ReactNode;
    onClose?: () => void;
}

export type FormType = {
    onSubmit: (data: FormData) => void;
    initialData?: any;
    onClose?: () => void;
};