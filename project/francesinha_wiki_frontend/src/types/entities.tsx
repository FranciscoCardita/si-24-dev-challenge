export type FrancesinhaType = {
    id: string;
    name: string;
    price: number;
    rating: number;
    photos: string;
    ingredients: IngredientType[];
    restaurant: RestaurantType;
};

export type IngredientType = {
    id: number;
    name: string;
};

export type RestaurantType = {
    id: number;
    name: string;
    address: string;
    city: string;
    country: string;
    rating: number;
    francesinhas: FrancesinhaType[];
};