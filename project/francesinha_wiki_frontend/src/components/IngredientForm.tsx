import { useForm } from "react-hook-form";

import { FormType } from "../types/components";
import Button from "./Button";

const IngredientForm = ({ onSubmit, initialData = {}, onClose }: FormType) => {
    const { register, handleSubmit, formState: { errors } } = useForm({
        defaultValues: initialData,
    });

    const onSubmitHandler = (data: any) => {
        const formData = new FormData();
        formData.append('name', data.name);

        onSubmit(formData);
        onClose && onClose();
    };

    return (
        <form onSubmit={handleSubmit(onSubmitHandler)} className="flex flex-col space-y-8">
            <div className='space-y-2'>
                <div>
                    <label className="label">Name</label>
                    <input {...register('name', { required: true })} type="text" className='input' />
                    {errors.name && <span>This field is required</span>}
                </div>
            </div>

            <Button type="submit" variant='contained'>Submit</Button>
        </form>
    );
}

export default IngredientForm