import { ButtonType } from "../types/components";

const getButtonVariant = (variant: string | undefined) => {
    switch (variant) {
        case 'contained':
            return 'btn-contained';
        case 'outlined':
            return 'btn-outlined';
        case 'text':
            return 'btn-text';
        default:
            return 'btn-text';
    }
}

const Button = ({ children, variant, className, onClick }: ButtonType) => {
    return (
        <button onClick={onClick} className={`${getButtonVariant(variant)} ${className}`}>
            {children}
        </button>
    );
}

export default Button;