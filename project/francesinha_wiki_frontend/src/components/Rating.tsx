import CircleIcon from "./CircleIcon";

const Rating = ({ rating = 0 }: { rating: number }) => {
    const filledCircles = Math.floor(rating);
    const hasHalfCircle = rating % 1 >= 0.5;

    return (
        <div className="flex items-center">
            {[...Array(5)].map((_, index) => {
                const currentRating = index + 1;
                const isFilled = filledCircles >= currentRating;
                const isHalfFilled = filledCircles + 1 === currentRating && hasHalfCircle;

                return (
                    <CircleIcon
                        key={index}
                        filled={isFilled}
                        halfFilled={isHalfFilled}
                        className="mr-1"
                    />
                );
            })}
            <span className="ml-2 text-[18px]">{rating}</span>
        </div>
    )
}

export default Rating;