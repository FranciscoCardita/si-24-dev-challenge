import { NavLink } from "react-router-dom";



const Navbar = () => {
    return (
        <nav className="border-b shadow-amber-500 z-20 sticky">
            <div className="layout flex justify-between items-center">
                <NavLink to="/" className="font-bold"> Francesinha Wiki 🥪 </NavLink>
                <div className="space-x-8">
                    <NavLink to="/francesinhas" className="hover:text-amber-600"> Francesinhas </NavLink>
                    <NavLink to="/restaurants" className="hover:text-amber-600"> Restaurants </NavLink>
                </div>
            </div>
        </nav>
    );
}

export default Navbar;