import { CircleIconType } from "../types/components";

const CircleIcon = ({ filled, halfFilled, className }: CircleIconType) => {
    if (halfFilled) {
        return (
            <div className={`relative w-6 h-6`}>
                <div className="absolute top-0.5 left-0 w-2.5 h-5 bg-emerald-400 rounded-l-full"></div>
                <div className={`absolute top-0.5 left-2.5 w-2.5 h-5 ${className} border-2 border-emerald-400 rounded-r-full`}></div>
            </div>
        );
    }

    return (
        <div className={`w-5 h-5 ${filled ? 'bg-emerald-400' : 'border-2 border-emerald-400'} rounded-full ${className}`}></div>
    );
};

export default CircleIcon;