import { CardProps } from "../types/components"

import Rating from "./Rating"

const Card = ({ title, description, rating = 0, image, onClick }: CardProps) => {
    return (
        <div className="card">
            {image && <img className="w-full h-56 object-cover rounded-sm" src={image} alt={title} />}
            <div className="card-body">
                <h4 className="whitespace-nowrap overflow-hidden">{title}</h4>
                <div className="space-y-2">
                    <div className="flex items-center">
                        <Rating rating={rating} />
                    </div>
                    <p className="text-lg whitespace-pre-line">{description}</p>
                </div>
            </div>
        </div>
    )
}

export default Card