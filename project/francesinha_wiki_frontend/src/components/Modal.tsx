import { ModalType } from "../types/components"
import Button from "./Button";

const Modal = ({ isOpen, title, children, onClose }: ModalType) => {
    if (!isOpen) return null;

    const handleOverlayClick = (event: Event) => {
        event.stopPropagation();
        onClose && onClose();
    }

    const handleModalClick = (event: Event) => {
        event.stopPropagation();
    }

    return (
        <div className="fixed inset-0 bg-gray-600/50 flex justify-center items-center z-40 backdrop-blur-sm" onClick={() => handleOverlayClick}>
            <div className="bg-white rounded-xl shadow-lg w-1/3" onClick={() => handleModalClick}>
                <div className="border-b p-4 flex justify-between items-center">
                    <h3>{title}</h3>
                    <Button onClick={onClose}>&times;</Button>
                </div>
                <div className="p-4">
                    {children}
                </div>
            </div>
        </div>
    )
}

export default Modal