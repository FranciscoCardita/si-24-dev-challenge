import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import { FormType } from '../types/components';
import Button from './Button';
import { RestaurantType, IngredientType } from '../types/entities';
import { getRestaurants, getIngredients } from '../api/api';

const FrancesinhaForm = ({ onSubmit, initialData = {}, onClose }: FormType) => {
    const [restaurants, setRestaurants] = useState<RestaurantType[]>([]);
    const [ingredients, setIngredients] = useState<IngredientType[]>([]);
    const { register, handleSubmit, formState: { errors } } = useForm({
        defaultValues: initialData,
    });

    useEffect(() => {
        const listRestaurants = async () => {
            try {
                const { data } = await getRestaurants();
                setRestaurants(data);
            } catch {
                console.log('Error listing restaurants');
            }
        }
        const listIngredients = async () => {
            try {
                const { data } = await getIngredients();
                setIngredients(data);
            } catch {
                console.log('Error listing ingredients');
            }
        }
        listRestaurants();
        listIngredients();
    }, []);

    const onSubmitHandler = (data: any) => {
        const formData = new FormData();
        formData.append('name', data.name);
        formData.append('price', data.price);
        formData.append('rating', data.rating);

        console.log(data.restaurant);
        console.log(initialData.restaurant);

        if (initialData?.restaurant === undefined ||
            (data?.restaurant?.id === undefined && initialData.restaurant?.id !== data?.restaurant)) {
            formData.append('restaurant', data.restaurant.toString());
        }

        const selectedIngredients = ingredients;
        for (let ingredient of selectedIngredients) {
            if (data.ingredients[ingredient.name]) {
                formData.append('ingredients', ingredient.id.toString());
            }
        }

        if (data.photos?.length !== 0 && (data.photos !== initialData?.photos)) {
            formData.append('photos', data.photos[0]);
        }

        onSubmit(formData);
        onClose && onClose();
    };

    return (
        <form onSubmit={handleSubmit(onSubmitHandler)} className="flex flex-col space-y-8">
            <div className='space-y-2'>
                <div>
                    <label className="label">Name</label>
                    <input {...register('name', { required: 'Name is required' })} className='input' />
                    {errors.name && <p className='input-error'>Invalid name</p>}
                </div>

                <div>
                    <label className="label">Price</label>
                    <input type="number" step="0.01" min="0" {...register('price', { required: 'Price is required' })} className='input' />
                    {errors.price && <p className='input-error'>Invalid price</p>}
                </div>

                <div>
                    <label className="label">Rating</label>
                    <input type="number" step="0.1" min="1" max="5" {...register('rating', { required: 'Rating is required' })} className='input' />
                    {errors.rating && <p className='input-error'>Rating must be between 1 and 5</p>}
                </div>

                <div>
                    <label className="label">Ingredients</label>
                    <div className="grid grid-cols-2 gap-x-4 ml-1">
                        {ingredients && ingredients.map((ingredient) => (
                            <div key={ingredient.id} className='flex justify-between items-center'>
                                <label>{ingredient.name}</label>
                                <input
                                    type="checkbox"
                                    {...register(`ingredients.${ingredient.name}`)}
                                    id={ingredient.id.toString()}
                                    defaultChecked={initialData.ingredients && initialData.ingredients.some((i: IngredientType) => i.id === ingredient.id)}
                                />
                            </div>
                        ))}
                    </div>
                </div>

                <div>
                    <label className="label">Restaurant</label>
                    <select {...register('restaurant', { required: 'Restaurant is required' })} className='input py-2'>
                        {initialData.restaurant ?
                            null :
                            <option value="" selected disabled hidden>Select a restaurant</option>
                        }
                        {restaurants.map((restaurant) => {
                            const isSelected = initialData.restaurant && restaurant.id === initialData.restaurant.id;
                            return (<option key={restaurant.id} value={restaurant.id} selected={isSelected}>{restaurant.name}</option>)
                        })}
                    </select>
                    {errors.restaurant && <p className='input-error'>Invalid restaurant</p>}
                </div>

                <div>
                    <label className="label">Photos</label>
                    <input id='fileInput' type="file" accept=".jpeg, .jpg, .png" {...register('photos')} multiple className='file-input' />
                </div>
            </div>

            <Button type="submit" variant='contained' className="w-full">Submit</Button>
        </form>
    )
}

export default FrancesinhaForm