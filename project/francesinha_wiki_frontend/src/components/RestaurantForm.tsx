import { useForm } from 'react-hook-form';

import { FormType } from '../types/components';
import Button from './Button';

export const RestaurantForm = ({ onSubmit, initialData = {}, onClose }: FormType) => {
    const { register, handleSubmit, formState: { errors } } = useForm({
        defaultValues: initialData,
    });

    const onSubmitHandler = (data: any) => {
        const formData = new FormData();
        formData.append('name', data.name);
        formData.append('address', data.address);
        formData.append('city', data.city);
        formData.append('country', data.country);
        formData.append('rating', data.rating);

        onSubmit(formData);
        onClose && onClose();
    };

    return (
        <form onSubmit={handleSubmit(onSubmitHandler)} className="flex flex-col space-y-8">
            <div className='space-y-2'>
                <div>
                    <label className="label">Name</label>
                    <input {...register('name', { required: true })} type="text" className='input' />
                    {errors.name && <span>This field is required</span>}
                </div>

                <div>
                    <label className="label">Address</label>
                    <input {...register('address', { required: true })} type="text" className='input' />
                    {errors.address && <span>This field is required</span>}
                </div>

                <div>
                    <label className="label">City</label>
                    <input {...register('city', { required: true })} type="text" className='input' />
                    {errors.address && <span>This field is required</span>}
                </div>

                <div>
                    <label className="label">Country</label>
                    <input {...register('country', { required: true })} type="text" className='input' />
                    {errors.address && <span>This field is required</span>}
                </div>

                <div>
                    <label className="label">Rating</label>
                    <input step='0.1' min='1' max='5' {...register('rating', { required: true })} type="number" className='input' />
                    {errors.rating && <span>This field is required</span>}
                </div>
            </div>

            <Button type="submit" variant='contained'>Submit</Button>
        </form>
    );
}

export default RestaurantForm;