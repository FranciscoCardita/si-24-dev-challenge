import { BrowserRouter, Navigate, Outlet, Route, Routes } from 'react-router-dom'

import Navbar from './components/Navbar'
import Home from './pages/Home'
import Francesinhas from './pages/Francesinhas';
import FrancesinhaDetails from './pages/FrancesinhaDetails';
import Ingredients from './pages/Ingredients';
import Restaurants from './pages/Restaurants';
import RestaurantDetails from './pages/RestaurantDetails';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          element={
            <>
              <Navbar />
              <Outlet />
            </>
          }
        >
          <Route path="*" element={<Navigate to="/" />} />
          <Route path="/" element={<Home />} />
          <Route path="/francesinhas" element={<Francesinhas />} />
          <Route path="/francesinhas/:id" element={<FrancesinhaDetails />} />
          <Route path="/francesinhas/ingredients" element={<Ingredients />} />
          <Route path="/restaurants" element={<Restaurants />} />
          <Route path="/restaurants/:id" element={<RestaurantDetails />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
