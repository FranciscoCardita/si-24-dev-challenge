import axios from "axios";

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
});

// Francesinhas
export const getFrancesinhas = async (search?: string, sort = 'id') => {
    return await api.get(`francesinhas/?ordering=${sort}${search ? `&search=${search}` : ""}`);
}

export const getFrancesinha = async (id: string) => {
    return await api.get(`francesinhas/${id}/`);
}

export const createFrancesinha = async (data: FormData) => {
    return await api.post("francesinhas/", data);
}

export const updateFrancesinha = async (id: string, data: FormData) => {
    return await api.patch(`francesinhas/${id}/`, data);
}

export const deleteFrancesinha = async (id: string) => {
    return await api.delete(`francesinhas/${id}/`);
}

// Restaurants
export const getRestaurants = async (search?: string, sort = 'id') => {
    return await api.get(`restaurants/?ordering=${sort}${search ? `&search=${search}` : ""}`);
}

export const getRestaurant = async (id: string) => {
    return await api.get(`restaurants/${id}/`);
}

export const createRestaurant = async (data: FormData) => {
    return await api.post("restaurants/", data);
}

export const updateRestaurant = async (id: string, data: FormData) => {
    return await api.patch(`restaurants/${id}/`, data);
}

export const deleteRestaurant = async (id: string) => {
    return await api.delete(`restaurants/${id}/`);
}

// Ingredients
export const getIngredients = async (search?: string) => {
    return await api.get(`francesinhas/ingredients/${search ? `?search=${search}` : ""}`);
}

export const getIngredient = async (id: string) => {
    return await api.get(`francesinhas/ingredients/${id}/`);
}

export const createIngredient = async (data: FormData) => {
    return await api.post("francesinhas/ingredients/", data);
}

export const updateIngredient = async (id: string, data: FormData) => {
    return await api.put(`francesinhas/ingredients/${id}/`, data);
}

export const deleteIngredient = async (id: string) => {
    return await api.delete(`francesinhas/ingredients/${id}/`);
}