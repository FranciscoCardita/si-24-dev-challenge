![logo](assets/logo.png)

>Discover the best Francesinhas and where to eat them. 🍽️

## Tech Stack

### Backend
- Django
- Django REST Framework
- PostgreSQL

### Frontend
- React
- TypeScript
- Tailwind CSS

## Requirements
- Docker/Docker Compose
- `make` command

## Getting Started

#### 0. Clone the repository and navigate to the project directory

```
git clone https://gitlab.com/FranciscoCardita/si-24-dev-challenge.git
cd si-24-dev-challenge/project
```

#### 1. Create a local `.env` file

```
make prepare-env
```

#### 2. Build and run container images

```
make build && make up
```

#### 3. Apply DB migrations

After the containers are up and running, open a new terminal and run the following command:

```
make db-makemigrations && make db-migrate
```

#### 4. Access the platform

Open your browser and navigate to `http://localhost:3000`.

## API Endpoints

### Francesinhas

- **Get All Francesinhas**
  ```plaintext
  GET /francesinhas/?ordering={sort}&search={search}
  ```
  Retrieves a list of Francesinhas with optional sorting and searching.

- **Get a Single Francesinha**
  ```plaintext
  GET /francesinhas/{id}/
  ```
  Retrieves details of a single Francesinha by ID.

- **Create a Francesinha**
  ```plaintext
  POST /francesinhas/
  ```
  Creates a new Francesinha with the provided data.

- **Update a Francesinha**
  ```plaintext
  PATCH /francesinhas/{id}/
  ```
  Updates an existing Francesinha with the provided data.

- **Delete a Francesinha**
  ```plaintext
  DELETE /francesinhas/{id}/
  ```
  Deletes a Francesinha by ID.

### Restaurants

- **Get All Restaurants**
  ```plaintext
  GET /restaurants/?ordering={sort}&search={search}
  ```
  Retrieves a list of Restaurants with optional sorting and searching.

- **Get a Single Restaurant**
  ```plaintext
  GET /restaurants/{id}/
  ```
  Retrieves details of a single Restaurant by ID.

- **Create a Restaurant**
  ```plaintext
  POST /restaurants/
  ```
  Creates a new Restaurant with the provided data.

- **Update a Restaurant**
  ```plaintext
  PATCH /restaurants/{id}/
  ```
  Updates an existing Restaurant with the provided data.

- **Delete a Restaurant**
  ```plaintext
  DELETE /restaurants/{id}/
  ```
  Deletes a Restaurant by ID.

### Ingredients

- **Get All Ingredients**
  ```plaintext
  GET /francesinhas/ingredients/?search={search}
  ```
  Retrieves a list of Ingredients with optional searching.

- **Get a Single Ingredient**
  ```plaintext
  GET /francesinhas/ingredients/{id}/
  ```
  Retrieves details of a single Ingredient by ID.

- **Create an Ingredient**
  ```plaintext
  POST /francesinhas/ingredients/
  ```
  Creates a new Ingredient with the provided data.

- **Update an Ingredient**
  ```plaintext
  PUT /francesinhas/ingredients/{id}/
  ```
  Updates an existing Ingredient with the provided data.

- **Delete an Ingredient**
  ```plaintext
  DELETE /francesinhas/ingredients/{id}/
  ```
  Deletes an Ingredient by ID.

## Screenshots
### Home Page
![Home Page](assets/home_page.png)

### Francesinhas Page
![Francesinhas Page](assets/francesinhas_page.png)

### Francesinha Detail Page
![Francesinha Page](assets/mega_francesinha_page.png)

### Ingredients Page
![Ingredients Page](assets/francesinhas_ingredients_page.png)

### Restaurants Page
![Restaurants Page](assets/restaurants_page.png)

### Restaurant Detail Page
![Restaurant Page](assets/a_biquinha.png)