from rest_framework import serializers
from .models import Francesinha, Ingredient, Restaurant

class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ['id', 'name']

class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['id', 'name']

class FrancesinhaSerializer(serializers.ModelSerializer):
    ingredients = IngredientSerializer(many=True, read_only=True)
    restaurant = RestaurantSerializer(read_only=True)
    class Meta:
        model = Francesinha
        fields = '__all__'

class FrancesinhaListSerializer(serializers.ModelSerializer):
    restaurant = RestaurantSerializer(read_only=True)

    class Meta:
        model = Francesinha
        fields = ['id', 'name', 'price', 'rating', 'restaurant', 'photos']

class FrancesinhaPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Francesinha
        fields = '__all__'
        read_only_fields = ['id']

class FrancesinhaPatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Francesinha
        fields = ['name', 'price', 'rating', 'ingredients', 'restaurant', 'photos']