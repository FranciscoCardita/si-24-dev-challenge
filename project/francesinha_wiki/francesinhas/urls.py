from django.urls import path
from .views import FrancesinhaListCreateView, FrancesinhaDetailView, IngredientListCreateView, IngredientDetailView

urlpatterns = [
    path('', FrancesinhaListCreateView.as_view(), name='francesinha-list-create'),
    path('<int:pk>/', FrancesinhaDetailView.as_view(), name='francesinha-detail'),
    path('ingredients/', IngredientListCreateView.as_view(), name='ingredient-list-create'),
    path('ingredients/<int:pk>/', IngredientDetailView.as_view(), name='ingredient-detail')
]