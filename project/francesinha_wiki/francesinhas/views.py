from rest_framework import generics, filters, status
from django.db import transaction
from rest_framework.response import Response
from rest_framework.response import Response
from .models import Francesinha, Ingredient
from .serializers import FrancesinhaSerializer, FrancesinhaPostSerializer, FrancesinhaPatchSerializer, FrancesinhaListSerializer, IngredientSerializer

class FrancesinhaListCreateView(generics.ListCreateAPIView):
    queryset = Francesinha.objects.all()
    serializer_class = FrancesinhaSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return FrancesinhaPostSerializer
        return FrancesinhaListSerializer

    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name', 'ingredients__name', 'restaurant__name']

class FrancesinhaDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Francesinha.objects.all()
    serializer_class = FrancesinhaSerializer

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return FrancesinhaPatchSerializer
        return FrancesinhaSerializer

class IngredientListCreateView(generics.ListCreateAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['name']

class IngredientDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer

    def destroy(self, request, *args, **kwargs):
        ingredient = self.get_object()

        with transaction.atomic():
            # Delete all francesinhas that contain this ingredient
            Francesinha.objects.filter(ingredients=ingredient).delete()

            ingredient.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)