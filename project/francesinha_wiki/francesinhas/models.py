from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from restaurants.models import Restaurant
from decimal import Decimal

class Ingredient(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Francesinha(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=5, decimal_places=2, validators=[MinValueValidator(Decimal(0))])
    rating = models.DecimalField(max_digits=3, decimal_places=1, validators=[MinValueValidator(Decimal(1)), MaxValueValidator(Decimal(5))])
    ingredients = models.ManyToManyField(Ingredient, related_name='francesinhas')
    photos = models.ImageField(upload_to='francesinhas_photos/', blank=True, null=True)
    restaurant = models.ForeignKey(Restaurant, related_name='francesinhas', on_delete=models.CASCADE)

    def __str__(self):
        return self.name