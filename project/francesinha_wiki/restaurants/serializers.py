from rest_framework import serializers
from .models import Restaurant
from francesinhas.models import Francesinha

class FrancesinhaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Francesinha
        fields = ['id', 'name']

class RestaurantSerializer(serializers.ModelSerializer):
    francesinhas = FrancesinhaSerializer(many=True, read_only=True)

    class Meta:
        model = Restaurant
        fields = '__all__'

class RestaurantListSerializer (serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['id', 'name', 'city', 'country', 'rating']