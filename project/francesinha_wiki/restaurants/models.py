from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from decimal import Decimal

class Restaurant(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    rating = models.DecimalField(max_digits=3, decimal_places=1, validators=[MinValueValidator(Decimal(1)), MaxValueValidator(Decimal(5))])

    def __str__(self):
        return self.name